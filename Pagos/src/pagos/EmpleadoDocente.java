/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

/**
 *
 * @author Andres
 */
public class EmpleadoDocente extends Empleado{
     private int nivel;
     private int horasLaboradas;
     private float pagoPorHora;

 public EmpleadoDocente(){
         this.nivel = 0;
         this.horasLaboradas = 0;
         this.pagoPorHora = 0.0f;
        }     
     
 public EmpleadoDocente(int numEmpleado, String nombre, String domicilio, Contrato contrato, int nivel, int horasLaboradas, float pagoPorHora) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLaboradas = horasLaboradas;
        this.pagoPorHora = pagoPorHora;
    }
    
   
 //Set & Get

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHorasLaboradas() {
        return horasLaboradas;
    }

    public void setHorasLaboradas(int horasLaboradas) {
        this.horasLaboradas = horasLaboradas;
    }

    public float getPagoPorHora() {
        return pagoPorHora;
    }

    public void setPagoPorHora(float pagoPorHora) {
        this.pagoPorHora = pagoPorHora;
    }
    
    public float calcularAdicional() {
        float adicional = 0;
        switch (nivel) {
            case 1:
                adicional = horasLaboradas * pagoPorHora * 0.35f;
                break;
            case 2:
                adicional = horasLaboradas * pagoPorHora * 0.40f;
                break;
            case 3:
                adicional = horasLaboradas * pagoPorHora * 0.50f;
                break;
        }
        return adicional;
    }

    @Override
    public float calcularTotal() {
       float pagoBase = horasLaboradas * pagoPorHora;
        float pagoAdicional = calcularAdicional();
        return pagoBase + pagoAdicional;
    }

    @Override
    public float calcularImpuesto() {
      return calcularTotal() * (contrato.getImpuestoIsr() / 100);
    }
}
