/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagos;

/**
 *
 * @author Andres
 */
public class Contrato extends Empleado {
         private String claveContrato;
         private String puesto;
         private float impuestoIsr;
        
    public Contrato(String claveContrato, String puesto, float impuestoISR) {
        this.claveContrato = claveContrato;
        this.puesto = puesto;
        this.impuestoIsr = impuestoISR;
    }
        
        public Contrato(){
            this.claveContrato = "";
            this.puesto="";
            this.impuestoIsr=0.0f;
        }
    
    //Set & Get

    public String getClaveContrato() {
        return claveContrato;
    }

    public void setClaveContrato(String claveContrato) {
        this.claveContrato = claveContrato;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoIsr() {
        return impuestoIsr;
    }

    public void setImpuestoIsr(float impuestoIsr) {
        this.impuestoIsr = impuestoIsr;
    }
    

    @Override
    public float calcularTotal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float calcularImpuesto() {
      this.impuestoIsr = impuestoIsr;
      return impuestoIsr;
    }
    
    
}
